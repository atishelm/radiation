import subprocess
from subprocess import Popen, PIPE
import time
import os
from six.moves import input
import re
import datetime
from Initialize import chip_dict

def record_single_chip(filename,chip,tid):
    os.chdir("/home/grosin/Radiation/"+chip)
    print(os.getcwd())
    os.chdir("itsdaq-sw")
    print(os.getcwd())
    p = Popen("source ../activate.sh && source RUNITSDAQ.sh",stdin=PIPE,shell=True,executable="/bin/bash")
    p.stdin.write(("f->writeMonitorData(\""+str(filename)+"\","+str(tid)+");\n").encode())
    p.communicate(".q".encode())
    os.system("cp -p "+filename+" /home/grosin/google-drive/")


def x_time(begin,now):
    return float((now-begin)/datetime.timedelta(seconds=1))

def get_tid(dose):
    start_time=datetime.datetime(2019,2,4,14,9,30)
    now=datetime.datetime.now()
    passed_time=x_time(start_time,now)
    return float(passed_time)*dose/3600.0

def loop_chips(filename):
    chips=["ABCStar11","ABCStar12","ABCStar13","ABCStar14"]
    for chip in chips:
        tid=get_tid(float(chip_dict["Chip1"+chip[-1]].dose))
        data_file=filename+chip+"_fmc_data.txt"
        record_single_chip(data_file,chip,tid)
        os.chdir("/home/grosin/Radiation")
        time.sleep(5)
        
if __name__=="__main__":
    loop_chips("fmc_data")
